import java.util.Arrays;
import java.util.Scanner;

public class HangMan {
	private static final boolean testingMode = true; 
	//enable or disable showing of the word

	
	public static void main(String[] args) {
		boolean userSolvedWord = false;
		int numberOfTimesPlayed = 0;
		Scanner sc = new Scanner(System.in);
		//main scanner for user input
		difficultySelection(sc,generateWord(), numberOfTimesPlayed, userSolvedWord);
		//the method that requests the difficulty
	}
	
	
	public static String generateWord() {
		return RandomWord.newWord();
	}
	
	
	public static void difficultySelection(Scanner sc, String generatedWord,int numberOfTimesPlayed, boolean userSolvedWord) {
		

		System.out.println("Enter your difficulty: Easy (e), Intermediate (i),"
				+ "or Hard (h)");
		String difficulty = sc.nextLine();
		// allows us to see the secret word for testing
		if (testingMode == true) {
			System.out.println("The secret word is: " + generatedWord);
		}
		//moved the userGuessedWord generation to here to prevent re-initialization during failed guesses.
		String[] userGuessedWord = new String[generatedWord.length()];
		for (int i = 0; i < generatedWord.length(); i++) {
			userGuessedWord[i] = "-";
		}
		//moved this statement out of the method that takes guesses
		String userGuessedWordString = "";
		for (int i = 0; i < userGuessedWord.length; i++) {
			userGuessedWordString = userGuessedWordString + userGuessedWord[i];
		}
		System.out.println("The word is: " + userGuessedWordString);
		
		if(difficulty.contains("e") && !difficulty.contains("i") && 
				!difficulty.contains("h")){
			//checks to see if "e" was entered"
			int numberOfGuesses = 15;
			int numberOfSpaces = 4;
			guessing(sc,generatedWord,numberOfGuesses,numberOfSpaces,userGuessedWord, numberOfTimesPlayed, userSolvedWord);
		}
		else if(difficulty.contains("i") && !difficulty.contains("e") && 
				!difficulty.contains("h")) {
			//checks to see if "i" was entered
			int numberOfGuesses = 12;
			int numberOfSpaces = 3;
			guessing(sc,generatedWord,numberOfGuesses,numberOfSpaces, userGuessedWord, numberOfTimesPlayed, userSolvedWord);
		}
		else if(difficulty.contains("h") && !difficulty.contains("e") && 
				!difficulty.contains("i")) {
			//checks to see if "h" was entered
			int numberOfGuesses = 10;
			int numberOfSpaces = 2;
			guessing(sc,generatedWord,numberOfGuesses,numberOfSpaces, userGuessedWord, numberOfTimesPlayed, userSolvedWord);
		}
		else{
			//if none of the others were entered:
			System.out.println("Invalid difficulty. Try Again...");
			difficultySelection(sc,generatedWord, numberOfTimesPlayed, userSolvedWord);
		}
	}
	
	
	public static void guessing(Scanner sc, String generatedWord, int numberOfGuesses, int numberOfSpaces, String[] userGuessedWord, int numberOfTimesPlayed, boolean userSolvedWord) {
		
		// checks to see if the user ran out of guesses
		if(numberOfGuesses == 0) {
			//user ran out of guesses
			System.out.println("You have failed to guess the word... :(");
			System.out.println("Would you like to play again? Yes (y) or No (n)");
			String playAgain = sc.nextLine();
			numberOfTimesPlayed += 1;
			if (numberOfTimesPlayed >= 20) {
				System.out.println("Sorry, you've exceeded the amount of times you can play! The program will now exit");
				System.exit(0);
			}
			//Checks user input for errors for continuation of the game
			while(true) {
				if(playAgain.equalsIgnoreCase("y")){
					difficultySelection(sc,generateWord(), numberOfTimesPlayed, userSolvedWord);
				}
				else if(playAgain.equalsIgnoreCase("n")){
					System.exit(0);
				} else {
					System.out.println("Your input is not valid. Would you like to play again? Yes (y) or No (n)");
					playAgain = sc.nextLine();
				}
			}
		} else if(!Arrays.toString(userGuessedWord).contains("-")) {
			// checks to see if the user guessed the word correctly
			System.out.println("You have guessed the word! Congratulations");
			System.out.println("Would you like to play again? Yes (y) or No (n)");
			String playAgain = sc.nextLine();
			numberOfTimesPlayed += 1;
			if (numberOfTimesPlayed >= 20) {
				System.out.println("Sorry, you've exceeded the amount of times you can play! The program will now exit");
				System.exit(0);
			}
			//Checks user input for errors for continuation of the game
			while (true) {
				if(playAgain.equalsIgnoreCase("y")){
					difficultySelection(sc,generateWord(), numberOfTimesPlayed, userSolvedWord);
				}
				else if (playAgain.equalsIgnoreCase("n")){
					System.exit(0);	
				} else {
					System.out.println("Your input is not valid. Would you like to play again? Yes (y) or No (n)");
					playAgain = sc.nextLine();
					
				}
			}	
		} else if(userSolvedWord == true) {
			//resets the boolean condition of whether the user correctly hard guess the word
			userSolvedWord = false;
			System.out.println("You have guessed the word! Congratulations!");
			System.out.println("Would you like to play again? Yes (y) or No (n)");
			String playAgain = sc.nextLine();
			numberOfTimesPlayed += 1;
			if (numberOfTimesPlayed >= 20) {
				System.out.println("Sorry, you've exceeded the amount of times you can play! The program will now exit");
				System.exit(0);
			}
			//Checks user input for errors for continuation of the game
			while (true) {
				if(playAgain.equalsIgnoreCase("y")){
					difficultySelection(sc,generateWord(), numberOfTimesPlayed, userSolvedWord);
				}
				else if (playAgain.equalsIgnoreCase("n")){
					System.exit(0);	
				} else {
					System.out.println("Your input is not valid. Would you like to play again? Yes (y) or No (n)");
					playAgain = sc.nextLine();
					
				}
			}	
		}
		
		
		System.out.println("Please enter the letter you want to guess:");
		String guessedLetter = sc.nextLine();
		
		if (guessedLetter.equalsIgnoreCase("solve")) {
			String[] guessedPlacesArr = new String[1];
			// if user enters solve then program allows user to attempt to solve
			guessChecker(sc,numberOfGuesses, guessedPlacesArr, guessedLetter, userGuessedWord,generatedWord,  numberOfSpaces, numberOfTimesPlayed);

		}
		// if more than one character is entered, only looks at the first character
		if(guessedLetter.length() > 1) {
			guessedLetter = guessedLetter.charAt(0) + "";
		} 
		
		//checks to make sure only characters were entered by user for guessing
		if(!guessedLetter.matches("[a-zA-Z]+")) {
			System.out.println("Your input is not valid. Try again.");
			System.out.println("Guesses remaining: " + numberOfGuesses);
			guessing(sc,generatedWord,numberOfGuesses,numberOfSpaces, userGuessedWord, numberOfTimesPlayed, userSolvedWord);
		}
		
		System.out.println("Please enter the spaces you would like to check (seperated by spaces):");
		String guessedPlacesString = sc.nextLine();
		//creates an array of the guesses that is split
		String[] guessedPlacesArr = guessedPlacesString.split("\\s+");
		
		//checks to make sure there aren't too many guesses or too little
		if(guessedPlacesArr.length != numberOfSpaces) {
			System.out.println("Your input is not valid. Try again");
			System.out.println("Guesses remaining: " + numberOfGuesses);
			guessing(sc,generatedWord,numberOfGuesses,numberOfSpaces, userGuessedWord, numberOfTimesPlayed, userSolvedWord);
		}
		
		// more error checking
		for(int i = 0; i < guessedPlacesArr.length; i++) {
			if (guessedPlacesArr[i].matches("[a-zA-Z]+")) { 
				//checks to see if the user enters a letter instead of an int
				System.out.println("Your input is not valid. Try again.");
				System.out.println("Guesses remaining: " + numberOfGuesses);
				guessing(sc,generatedWord,numberOfGuesses,numberOfSpaces, userGuessedWord, numberOfTimesPlayed, userSolvedWord);
			}
			else if(Integer.parseInt(guessedPlacesArr[i]) >= generatedWord.length() || Integer.parseInt(guessedPlacesArr[i]) < 0) {
				//tests to see if they input a number too high or less than 0
				System.out.println("Your input is not valid. Try again.");
				System.out.println("Guesses remaining: " + numberOfGuesses);
				guessing(sc,generatedWord,numberOfGuesses,numberOfSpaces, userGuessedWord, numberOfTimesPlayed, userSolvedWord );
			}
		}
		
		guessChecker(sc,numberOfGuesses, guessedPlacesArr, guessedLetter, userGuessedWord,generatedWord,  numberOfSpaces, numberOfTimesPlayed);

	}
	public static void guessChecker(Scanner sc,int numberOfGuesses, String[] guessedPlacesArr, String guessedLetter, String[] userGuessedWord, String generatedWord, int numberOfSpaces, int numberOfTimesPlayed) {
		boolean userSolvedWord = false;
		if (guessedLetter.equalsIgnoreCase("solve")) {
			System.out.println("Please solve the word:");
			String userSolve = sc.nextLine();
			if (userSolve.equals(generatedWord)) {
				System.out.println("You win!");
				userSolvedWord = true;
				guessing(sc,generatedWord,numberOfGuesses,numberOfSpaces, userGuessedWord, numberOfTimesPlayed, userSolvedWord);
			} else {
				numberOfGuesses--;
				System.out.println("That is not the secret word");
				System.out.println("Guessed remaining: " + numberOfGuesses);
				guessing(sc,generatedWord,numberOfGuesses,numberOfSpaces, userGuessedWord, numberOfTimesPlayed, userSolvedWord);

			}
			
		}
			int correctGuesses = 0;
				//counter to make sure at least one guess is right
			for(int i=0; i<guessedPlacesArr.length; i++ ) {
				int temp = Integer.parseInt(guessedPlacesArr[i]);
				//for loop for going through all the user guess spaces
				if(generatedWord.charAt(temp) == guessedLetter.charAt(0)) {
					//if statement checks the place in the word against the user guess
					userGuessedWord[temp] = "" + guessedLetter.charAt(0);
					//sets the guessed word to the new letter if its correct
					correctGuesses +=1;
					//adds one to correct guesses to show at least one guess was right	
				}
			}
				
			//Conditional to check if the user either guessed the letter correctly or did not
			if(correctGuesses > 0) {
				//if they got a guess right
				System.out.println("Your Guess is in the word!");
					
				String userGuessedWordString = "";
				// Creates String of users guessed letters so we can print to the screen
				for (int i = 0; i < userGuessedWord.length; i++) {
					userGuessedWordString = userGuessedWordString + userGuessedWord[i];
				}
				System.out.println("The updated word is: " + userGuessedWordString);
				System.out.println("Guesses remaining: " + numberOfGuesses);
				guessing(sc,generatedWord,numberOfGuesses,numberOfSpaces, userGuessedWord, numberOfTimesPlayed, userSolvedWord);
			}
			else {
				numberOfGuesses--;
				System.out.println("Your guess was not found in the spaces provided.");
				System.out.println("Guesses remaining: " + numberOfGuesses);
				//removes a guess if the letter wasn't in the word
				guessing(sc,generatedWord,numberOfGuesses,numberOfSpaces, userGuessedWord, numberOfTimesPlayed, userSolvedWord);
			}
	}
}
